import os
import json
import requests # to use the request OMBDB API
import ENV
from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy

basedir = os.path.abspath(os.path.dirname(__file__)) ## will get the full base path /c/user/rasha/desktop/

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] ='sqlite:///' + os.path.join(basedir, ENV.DB_PATH)
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///movies.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


db = SQLAlchemy(app)


class Movie(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    Title = db.Column(db.String(100), nullable=True)
    Year = db.Column(db.String(100), nullable=True)
    Rated = db.Column(db.String(100), nullable=True)
    Released = db.Column(db.String(100), nullable=True)
    Director = db.Column(db.String(100), nullable=True)
    Writer = db.Column(db.String(100), nullable=True)
    Actors = db.Column(db.String(100), nullable=True)

# to avoid python-BaseException:
# create db is a function that need the current app so we need run it with a context
def init_db():
    with app.app_context():
        # db.drop_all()
        db.create_all()


@app.route('/insertDataByTitle')
def insert_data_by_title():
    list_title = ["Star Wars", "game of thrones", "Fast and Furious"]
    list_to_add_db = []
    url = 'http://www.omdbapi.com/?apikey='+ENV.API_KEY
    for title in list_title:
        params = {
            't': title
        }
        response = requests.get(url, params=params).json()
        list_to_add_db.append(
            Movie(
                Title=title, 
                Year=response['Year'],
                Rated=response['Rated'],
                Released=response['Released'],
                Director=response['Director'],
                Writer=response['Writer'],
                Actors=response['Actors']
            )
        )
    db.session.add_all(list_to_add_db)
    db.session.commit()
    return "Done"


@app.route('/getMovies', methods=['POST'])
def get_movies():
    data = request.get_json()
    offset = data['offset']
    limit = data['limit']

    movies = Movie.query.offset(offset).limit(limit).all()
    print(movies)
    return "test"


if __name__ == "__main__":
    init_db()
    app.run(debug=True)